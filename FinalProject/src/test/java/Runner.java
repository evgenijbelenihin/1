import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {"src/test/resources/FinalProject"},
    /*glue = {"src/test/java/Steps"},*/
    plugin = {"pretty"},strict = true)
public class Runner {
}
