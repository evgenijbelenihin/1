package Steps;

import static Core.UMarkets.WebdriverSingleton.getDriver;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import java.util.concurrent.TimeUnit;

public class Hooks {

        @Before
        public void BeforeSteps() {
            System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
            getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        }

        @After
        public void AfterSteps() {
            getDriver().quit();
        }

    }
