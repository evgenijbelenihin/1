package Steps;

import static Core.UMarkets.WebdriverSingleton.getDriver;

import Core.UMarkets.LoginPage;
import Core.UMarkets.MainPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class LoginSteps {
    String expectedUserEmail;

    @Given("I open login page")
    public void iOpenLoginPage() {
        new LoginPage().openPage();
    }

    @When("^I fill in e-mail field ([^\"]*)$")
    public void iFillInEMailField(String UserEmail) {
        new LoginPage().fillEmailField(UserEmail);
        expectedUserEmail = UserEmail;
    }

    @And("^I fill in password field ([^\"]*)$")
    public void iFillInPasswordField(String UserPassword) {
        new LoginPage().fillPasswordField(UserPassword);
    }

    @And("I click Login button")
    public void iClickLoginButton() {
        new LoginPage().clickLoginButton();
    }

    @Then("I successfully logged in to the trading platform")
    public void iSuccessfullyLoggedInToTheTradingPlatform() {
        String actualUserEmail = new MainPage().getUserTitle();
        Assert.assertTrue("User title on main page equals user email", actualUserEmail.equals(expectedUserEmail));
        getDriver().quit();
    }
}
