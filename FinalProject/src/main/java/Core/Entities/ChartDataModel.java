package Core.Entities;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChartDataModel {
        private String              security;
        private String      barType;
        private List<WSBar> bars;
    }

