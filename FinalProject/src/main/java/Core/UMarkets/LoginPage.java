package Core.UMarkets;

import static Core.UMarkets.WebdriverSingleton.getDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {
    String LoginPageUrl = "https://dev-trading.umarkets.com/login.html";

    @FindBy(how = How.CSS, using = "[data-at=\"at-login-page-login\"]")
    private WebElement EmailField;
    @FindBy(how = How.CSS, using = "[data-at=\"at-login-page-password\"]")
    private WebElement PasswordField;
    @FindBy(how = How.CSS, using = "[data-at=\"at-login-page-login-button\"]")
    private WebElement LoginButton;

    public LoginPage(){
        PageFactory.initElements(getDriver(),this);
    }

    public LoginPage openPage(){
        getDriver().get(LoginPageUrl);
        return new LoginPage();
    }

    public void clickLoginButton(){
        LoginButton.click();
    }

    public void fillEmailField(String email){
        EmailField.sendKeys(email);
    }

    public void fillPasswordField(String password){
        PasswordField.sendKeys(password);
    }










}
