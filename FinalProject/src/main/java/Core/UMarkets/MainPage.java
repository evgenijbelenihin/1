package Core.UMarkets;

import static Core.UMarkets.WebdriverSingleton.getDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MainPage {

        @FindBy(how = How.CSS, using = "[data-at=\"at-header-welcome-account-name\"]")
        private WebElement UserTitle;

        public MainPage(){
            PageFactory.initElements(getDriver(),this);
        }

        public String getUserTitle(){
            return UserTitle.getText();
        }

    }

